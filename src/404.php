<?php
// Author: Kyohei Maeda
$path = realpath(dirname(__FILE__) . '') . "/";
include_once($path.'app_config.php');
include($path.'libs/meta.php');
?>
</head>

<body id="notfound">


<!-- Wrap
======================================================================-->
<div id="wrap">
	<main>
		<section>
			<h1>404</h1>
			<p>Page Not Found.    </p>
		</section>
	</main>
</div>



<!-- Scripts
======================================================================-->
<?php include($path.'libs/scripts.php'); ?>

</body>
</html>
