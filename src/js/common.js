$(function() {



	// Toggle Menu
	// -----------------------------------------------------------------------
	var currentScroll;
	$('#gnav-toggle').click(function() {
		$(this).toggleClass('is-active');
		$('#gnav').toggleClass("is-active");
		if($('#gnav-toggle').hasClass('is-active')) {
			currentScroll = $(window).scrollTop();
			$('#wrap').css( {
				position: 'fixed',
				width: '100%',
				top: -1*currentScroll
			});
		} else {
			$('#wrap').attr( { style: '' } );
			$(window).scrollTop(currentScroll);
		}
	});



	// fix header
	// -----------------------------------------------------------------------
	$(window).on("load resize", function(){
		var headerTop = $("#header").offset().top;
		var headerHeight = $("#header").outerHeight();
		$(window).on("scroll", function(){
			var scrollTop = $(window).scrollTop();
			if(scrollTop > headerTop){
				$('#header').addClass("fixed");
			} else {
				$('#header').removeClass("fixed");
			}
		});
	});



	// Accordion
	// -----------------------------------------------------------------------
	$('.toggle-menu').click(function() {
		$(this).toggleClass('opened');
		$(this).next('.toggle-content').slideToggle();
	});



});
