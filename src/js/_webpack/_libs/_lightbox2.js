// lightbox2
// https://www.npmjs.com/package/lightbox2
// http://lokeshdhakar.com/projects/lightbox2
//==================================================

// Load JS (From node_module)
var lightbox = require('lightbox2');

// Load CSS (From node_module)
require('lightbox2/dist/css/lightbox.css');

// Settings
// $(function() {
// 	lightbox.option({
		// 'alwaysShowNavOnTouchDevices': false,
		// 'disableScrolling': false,
		// 'fadeDuration': 600,
		// 'fitImagesInViewport': true,
		// 'imageFadeDuration': 600,
		// 'maxWidth': 100,
		// 'maxHeight': 100,
		// 'positionFromTop': 50,
		// 'resizeDuration': 700,
		// 'wrapAround': false
// 	});
// });
