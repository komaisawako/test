// Smooth Scroll
// https://www.npmjs.com/package/jquery-smooth-scroll
//==================================================
require('jquery-smooth-scroll');

// Settings
$(function() {
	$('a[href^="#"]').on('click', function() {
		var href= $(this).attr("href");
		var target = $(href == "#" || href === "" ? 'html' : href);
		$.smoothScroll({
			offset: 0, //スクロールをずらす距離
			// direction: 'top', //  'top'または 'left'
			easing: 'easeInOutCubic', //スクロールのアニメーション
			speed: 1000, //スクロールのスピード
			// autoCoefficient: 2,
			// preventDefault: true,
			scrollTarget: target, //スクロール先
			// delegateSelector: null,
			exclude: ['.no-smooth'], //スムーススクロールを適用しない要素
			beforeScroll: function() {}, //スクロール前に何かする
			afterScroll: function() {}, //スクロール後に何かする
		});
	});
});
