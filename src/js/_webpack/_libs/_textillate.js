// textlate
// https://www.npmjs.com/package/textillate
// http://textillate.js.org/
//==================================================

// Load JS (From node_module)
require('textillate/assets/jquery.lettering.js');
require('textillate');

// Load CSS (From node_module)
require('textillate/assets/animate.css');

// Settings
// $(function() {
// 	if(('.tlt').length){
// 		$('.tlt').textillate({
// 			loop: false,
// 			minDisplayTime: 2000,
// 			initialDelay: 0,
// 			autoStart: true,
// 			inEffects: [],
// 			outEffects: [ 'hinge' ],
// 			in: {
// 				effect: 'fadeInLeftBig',
// 				delayScale: 1.5,
// 				delay: 50,
// 				sync: false,
// 				shuffle: false,
// 				reverse: false,
// 				callback: function () {}
// 			},
// 			out: {
// 				effect: 'hinge',
// 				delayScale: 1.5,
// 				delay: 50,
// 				sync: false,
// 				shuffle: false,
// 				reverse: false,
// 				callback: function () {}
// 			},
// 			callback: function () {},
// 			type: 'char'
// 		});
// 	}
// });
