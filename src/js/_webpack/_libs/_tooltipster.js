// tooltipster
// [ NPM ] https://www.npmjs.com/package/tooltipster
// [ Docs ] http://iamceege.github.io/tooltipster/#options
//==================================================

// Load JS (From node_module)
var tooltipster = require('tooltipster');

// Load CSS (From node_module)
require('tooltipster/dist/css/tooltipster.bundle.min.css');
// Themes
require('tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css');
// require('tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css');
// require('tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-noir.min.css');
// require('tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-punk.min.css');
// require('tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-shadow.min.css');

// Settings
// $(function() {
// 	$('.tooltip').tooltipster({
// 		animation: 'fade',
// 		delay: 200,
// 		trigger: 'click',
// 		theme: 'tooltipster-borderless',
// 	});
// });
