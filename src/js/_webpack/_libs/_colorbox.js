// lightbox2
// https://www.npmjs.com/package/jquery-colorbox
// http://www.jacklmoore.com/colorbox/
//==================================================

// Load JS (From node_module)
require('jquery-colorbox');

// Load CSS (From node_module)
// require('jquery-colorbox/example1/colorbox.css');
require('jquery-colorbox/example2/colorbox.css');
// require('jquery-colorbox/example3/colorbox.css');
// require('jquery-colorbox/example4/colorbox.css');
// require('jquery-colorbox/example5/colorbox.css');

// Settings
// $(function() {
// $(".colorbox").colorbox({
// 		transition: "elastic",
// 		speed: 350,
// 		href: false,
// 		title: false,
// 		rel: false,
// 		scalePhotos: true,
// 		scrolling: true,
// 		opacity: 0.85,
// 		open: false,
// 		returnFocus: true,
// 		trapFocus: true,
// 		fastIframe: true,
// 		preloading: true,
// 		overlayClose: true,
// 		escKey: true,
// 		arrowKey: true,
// 		loop: true,
// 		data: false,
// 		className: false,
// 		fadeOut: 300,
// 		closeButton: true,
//
// 		current: "image {current} of {total}",
// 		previous: "previous",
// 		next: "next",
// 		close: "close",
// 		xhrError: "This content failed to load.",
// 		imgError: "This image failed to load.",
//
// 		iframe: false,
// 		inline: false,
// 		html: false,
// 		photo: false,
// 		ajax: false,
//
// 		width: false,
// 		height: false,
// 		innerWidth: false,
// 		innerHeight: false,
// 		initialWidth: 300,
// 		initialHeight: 100,
// 		maxWidth: false,
// 		maxHeight: false,
//
// 		slideshow: false,
// 		slideshowSpeed: 2500,
// 		slideshowAuto: true,
// 		slideshowStart: "start slideshow",
// 		slideshowStop: "stop slideshow",
//
// 		fixed: false,
// 		top: false,
// 		bottom: false,
// 		left: false,
// 		right: false,
// 		reposition: true,
//
// 		retinaImage: false,
// 		retinaUrl: false,
// 		retinaSuffix: "@2x.$1",
// 		onOpen: false,
// 		onLoad: false,
// 		onComplete: false,
// 		onCleanup: false,
// 		onClosed: false,
// 	});
// });
