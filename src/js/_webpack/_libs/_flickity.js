// flickity
// https://www.npmjs.com/package/flickity
//==================================================

// Load JS (From node_module)
global.Flickity = require('flickity');

// Load CSS (From node_module)
require('flickity/dist/flickity.min.css');

// Settings
// $(function() {
// 	var flkty = new Flickity( '.flickity', {
// 		wrapAround: true,
// 		prevNextButtons: true,
// 		pageDots: true,
// 		autoPlay: 4000,
// 	});
// });
