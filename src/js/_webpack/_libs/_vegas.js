// vegas
// https://www.npmjs.com/package/vegas
// http://vegas.jaysalvat.com/documentation/setup/
//==================================================

// Load JS (From node_module)
var vegas = require('vegas');

// Load CSS (From node_module)
require('vegas/dist/vegas.min.css');

// Settings
(function($){
	$(function() {
		$('body').vegas({
			animation: ['kenburns'],
			delay: 10000,
			transitionDuration: 5000,
			animationDuration: 20000,
		    timer: true,
		    shuffle: true,
		    firstTransition: 'fade',
		    firstTransitionDuration: 5000,
		    transition: 'slideDown2',
			slides: [
				{ src: 'images/photo01.jpg' },
				{ src: 'images/photo02.jpg' },
				{ src: 'images/photo03.jpg' }
			]
    	});
	});
})(jQuery);
