// slick-carousel
// https://www.npmjs.com/package/slick-carousel
//==================================================

// Load JS (From node_module)
var slick = require('slick-carousel');

// Load CSS (From node_module)
require('slick-carousel/slick/slick.css');
require('slick-carousel/slick/slick-theme.css');

// Settings
// $(function() {
// 	if(('.slick').length){
// 		$('.slick').slick({
// 			accessibility: true,
// 			autoplay: true,
// 			autoplaySpeed: 4000,
// 			speed: 1000,
// 			pauseOnHover: false,
// 			pauseOnDotsHover: false,
// 			cssEase: 'ease',
// 			dots: false,
// 			dotsClass: 'dot-class',
// 			draggable: true,
// 			fade: false,
// 			arrows: true,
// 			infinite: true,
// 			initialSlide: 0,
// 			swipe: true,
// 			// centerMode: true,
// 			// centerPadding: '160px',
// 			responsive: [{
// 				breakpoint: 1024,
// 				settings: {
// 					slidesToShow: 3,
// 					infinite: true
// 				}
// 			}, {
// 				breakpoint: 600,
// 				settings: {
// 					slidesToShow: 2,
// 					dots: true
// 				}
// 			}, {
// 				breakpoint: 300,
// 				settings: "unslick" // destroys slick
// 			}]
// 		});
// 	}
// });
