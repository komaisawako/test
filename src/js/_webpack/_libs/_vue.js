// vegas
// https://www.npmjs.com/package/vegas
// http://vegas.jaysalvat.com/documentation/setup/
//==================================================
window.Vue = require('vue/dist/vue.min.js');
new Vue({
	el: '#app',
	components: {
		MyMessage: require('./_vue_components/message.vue'),
	}
});
