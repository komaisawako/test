// jquery-inview
// https://www.npmjs.com/package/jquery-inview
//==================================================

// Load JS (From node_module)
require('jquery-inview');

// Settings
// $(function() {
// 	$('.inview').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
// 		if (isInView) {
// 			$(this).addClass('is-active');
// 		} else {
// 			$(this).removeClass('is-active');
// 		}
// 	});
// });
