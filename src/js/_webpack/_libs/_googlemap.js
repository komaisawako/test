// google-maps
// https://www.npmjs.com/package/google-maps
//==================================================
$(function() {

	// Load JS (From node_module)
	var GoogleMapsLoader = require('google-maps');

	//Settings
	GoogleMapsLoader.KEY = 'AIzaSyD5WNtRlGP2o-FDdZYIRIvQ5s3Q9Zy8yik'; // API Key
	GoogleMapsLoader.load(function(google) {
		// Map Style
		var styleArray = [{
				featureType: "all",
				stylers: [
			 		{ saturation: -100 }
				]
			},{
				featureType: "road.arterial",
				elementType: "geometry",
				stylers: [
					{ hue: "#00ffee" },
					{ saturation: 50 }
				]
			},{
				featureType: "poi.business",
				elementType: "labels",
				stylers: [
					{ visibility: "off" }
				]
			}
		];


		//Marker Setting
		var customMarker = {
			url : '/images/common/map-marker.png',
			scaledSize : new google.maps.Size(65, 70)
		};


		// Map Setting
		if($('#map-canvas').length){
			var myLatLng = {
				lat: 35.699119,
				lng: 139.733643
			};
			var map = new google.maps.Map(document.getElementById('map-canvas'), {
				center: myLatLng,
				scrollwheel: false,
				zoom: 18,
				styles: styleArray,
			});
			var marker = new google.maps.Marker({
				map: map,
				position: myLatLng,
				// icon: customMarker,
				title: ''
			});
		}


		// Reset Center
		google.maps.event.addDomListener(window, "resize", function() {
			var center = map.getCenter();
			google.maps.event.trigger(map, "resize");
			map.setCenter(center);
		});
	});
});
