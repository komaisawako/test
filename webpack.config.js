// USE webpack 2.3.2
// https://webpack.js.org/configuration/
var webpack = require('webpack');
var path = require('path');
var glob = require('glob');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var config = module.exports = function() {











	var jsBasePath = path.resolve(__dirname, 'src');
	var targets = glob.sync('./src/js/_webpack/*.js');
	var entries = {};
	targets.forEach(function (value) {
		var re = new RegExp('./src/js/_webpack/');
		var key = value.replace(re, '');
		entries[key] = value;
	});

	return {
		externals: {
			jquery: 'jQuery',
			$: 'jQuery',
		},

		entry: entries,
		output: {
			path: path.join(__dirname, 'dist/js'),
			filename: './js/[name]',
			publicPath: '../',
		},

		// context: __dirname + '/src',
		// entry: {
		// 	'bundle': './js/_webpack/index.js'
		// },
		// output: {
		// 	path: __dirname + '/dist',
		// 	filename: './js/[name].js',
		// 	publicPath: '../',
		// },

		module: {
			rules: [
				{
					test: /\.js$/,
					use: {
						loader: 'babel-loader'
					}
				},
				{
					test: /\.vue$/,
					use: {
						loader: 'vue-loader'
					}
				},
				{
					test: /\.(jpe?g|png|gif|svg|ico)(\?.+)?$/,
					// include: [
					// 	path.resolve(__dirname, 'src', 'img')
					// ],
					use: {
						loader: 'url-loader',
						options: {
							limit: 1,
							name: './images/[name].[ext]'
						}
					}
				},
				{
					test: /\.(eot|otf|ttf|woff2?|svg)(\?.+)?$/,
					// include: [
					// 	path.resolve(__dirname, 'node_modules')
					// ],
					use: {
						loader: 'file-loader',
						options: {
							name: './fonts/[name].[ext]'
						}
					}
				},
				{
					test: /\.(css|sass|scss)$/,
					use: ExtractTextPlugin.extract({
						fallback: 'style-loader',
						use:[
							'css-loader',
							'sass-loader',
							'postcss-loader'
						]
					})
				}

			]
		},
		plugins: [
			new webpack.ProvidePlugin({
				jQuery: 'jquery',
				$: 'jquery',
				jquery: "jquery",
				'window.jQuery': 'jquery'
			}),
			new webpack.LoaderOptionsPlugin({
				options: {
					postcss: [
						require('autoprefixer')({
							browsers: ['last 2 versions']
						})
					]
				}
			}),
			new ExtractTextPlugin({
				filename: './css/plugins.css',
				disable: false,
				allChunks: true
			}),
		],
		devtool: 'inline-source-map',
	};
};
